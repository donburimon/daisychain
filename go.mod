module donburimon/daisychain

go 1.12

require (
	github.com/google/go-cmp v0.5.1 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/stretchr/testify v1.6.1
	gotest.tools v2.2.0+incompatible
)
