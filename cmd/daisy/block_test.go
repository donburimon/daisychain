package daisy_test

import (
	"donburimon/daisychain/cmd/daisy"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewChain(t *testing.T) {
	assert := assert.New(t)

	node0 := &testData{
		NodeName: "node0",
	}
	node1 := &testData{
		NodeName: "node1",
	}
	blockChain := daisy.Genesis(node0)
	assert.Equal(1, len(blockChain.Blocks))
	assert.Equal(node0.Byte(), blockChain.Blocks[0].Data)
	assert.Equal(node0.Hash(), blockChain.Blocks[0].Hash)
	assert.Equal("GENESIS", blockChain.Blocks[0].Previous)

	blockChain = blockChain.Append(node1)
	assert.Equal(2, len(blockChain.Blocks))
	assert.Equal(node1.Byte(), blockChain.Blocks[1].Data)
	assert.Equal(node1.Hash(), blockChain.Blocks[1].Hash)
	assert.Equal(node0.Hash(), blockChain.Blocks[1].Previous)
}
