package daisy

import "time"

const (
	genesis = "GENESIS"
)

// Block struct
type Block struct {
	Hash      string
	Data      []byte
	Timestamp time.Time
	Previous  string // hash of previous block
}

func newBlock(data Data, previousHash *string) *Block {
	prev := genesis
	if previousHash != nil {
		prev = *previousHash
	}
	return &Block{
		Hash:      data.Hash(),
		Data:      data.Byte(),
		Timestamp: time.Now().UTC(),
		Previous:  prev,
	}
}
