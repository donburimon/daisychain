package daisy

// Data interface ...
type Data interface {
	Byte() []byte
	Hash() string
}
