package daisy

// definitons
var (
	Difficulty = 2
)

// Chain merkle tree implementation
type Chain struct {
	Blocks []*Block
}

// Genesis ...
func Genesis(data Data) *Chain {
	genesis := newBlock(data, nil)
	return &Chain{
		Blocks: []*Block{
			genesis,
		},
	}
}

// PoW Proof of work
func (me *Chain) PoW() {

}

// Append block
func (me *Chain) Append(data Data) *Chain {
	if len(me.Blocks) == 0 {
		return Genesis(data)
	}
	lastBlock := me.Blocks[len(me.Blocks)-1]
	me.Blocks = append(me.Blocks, newBlock(data, &lastBlock.Hash))
	return me
}
