package daisy_test

type testData struct {
	NodeName string
}

func (me *testData) Hash() string {
	return me.NodeName
}
func (me *testData) Byte() []byte {
	return []byte(me.NodeName)
}
