package bits_test

import (
	"donburimon/daisychain/cmd/bits"
	"testing"

	"gotest.tools/assert"
)

func Test_CountBits(t *testing.T) {
	number := 1
	bitsNo := bits.CountBits(number)
	assert.Equal(t, 1, bitsNo)
}

func Test_CountBitsBetween(t *testing.T) {
	from, to := 1, 4
	total := bits.CountBitsBetween(from, to)
	assert.Equal(t, 5, total)
}
