package bits

import (
	"math/bits"
)

// CountBits ... counts the number of "1"s in an integer
func CountBits(number int) int {
	return countbits(uint(number))
}

func countbits(number uint) int {
	return bits.OnesCount(number)
}

// CountBitsBetween total number of set bits between from and to
func CountBitsBetween(from, to int) (total int) {
	for i := from; i <= to; i++ {
		total += CountBits(i)
	}
	return total
}
